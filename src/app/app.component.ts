import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { take } from 'rxjs/operators';

export interface Asset {
  name: string;
  latitude: string;
  longitude: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'trackensure-test';
  activeIndex = 0;
  mapPosition: google.maps.LatLngLiteral = {
    lat: 0,
    lng: 0,
  }
  assetList: Asset[] = [
    {
      name: 'TestName1',
      latitude: '50.451864',
      longitude: '30.5981571'
    },
    {
      name: 'TestName2',
      latitude: '46.4881546',
      longitude: '30.7412108'
    }
  ]

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {
    this.setAssetPosition(this.assetList[0], 0);
  }

  updateAsset(index: number, isNew = false): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: {asset: !isNew ? this.assetList[index] : null}
    });
    dialogRef.afterClosed()
      .pipe(take(1))
      .subscribe((data: Asset) => {
        if (data) {
          if (!isNew) {
            this.assetList[index] = data;
            this.setAssetPosition(this.assetList[index], index);
          } else {
            this.assetList.push(data)
            this.setAssetPosition(this.assetList[this.assetList.length - 1], this.assetList.length - 1);
          }
        }
      })
  }

  setAssetPosition(asset: Asset, index: number): void {
    this.mapPosition = {lat: +asset.latitude, lng: +asset.longitude};
    this.activeIndex = index;
  }

  deleteAsset(index: number): void {
    this.assetList = this.assetList.filter((el, i) => i !== index)
  }
}
