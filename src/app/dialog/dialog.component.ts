import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Asset } from '../app.component';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

interface DialogData {
  asset: Asset;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  asset: Asset;
  formGroup: FormGroup;
  formControls = {
    name: new FormControl(null, Validators.required),
    latitude: new FormControl(null, [Validators.required, Validators.pattern(/^\d+\.?\d*$/)]),
    longitude: new FormControl(null, [Validators.required, Validators.pattern(/^\d+\.?\d*$/)])
  }

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private formBuilder: FormBuilder,
    ) {
    this.asset = this.data.asset
    this.formGroup = this.formBuilder.group(this.formControls)
    if (this.asset) {
      this.formGroup.setValue({
        name: this.asset.name,
        latitude: this.asset.latitude,
        longitude: this.asset.longitude,
      })
    }
  }

  ngOnInit(): void {

  }

  submit(): void {
    if (this.formGroup.valid) {
      this.dialogRef.close(this.formGroup.value as Asset)
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }


}
